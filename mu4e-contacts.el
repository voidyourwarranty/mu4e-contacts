;;; -*- mode:emacs-lisp -*-
;;; ================================================================================
;;; File:    mu4e-contacts.el
;;; Date:    2020-10-11
;;; Purpose: A contacts data base for mu4e using org-mode and helm.
;;; Author:  Void Your Warranty
;;; License: Gnu General Public License GPLv3 (https://www.gnu.org/licenses/gpl-3.0.en.html)
;;; Purpose: Contacts Management for the Emacs email system mu4e, using org-mode and helm.
;;; ================================================================================

(require 'mu4e)
(require 'org-mu4e)

(require 'helm)
(require 'helm-config)

;; Variables used by the contacts management.
;; ================================================================================

;; Variables to be configured by the user.
;; --------------------------------------------------------------------------------

(defcustom mc-contacts-files nil
  "The list of all files that comprise the contacts data base.")

(defcustom mc-capture-file nil
  "One of the contacts files that has a 'New Contacts' section where captured contacts are placed")

(defcustom mc-compose-complete-only-after "1900-01-01 00:00:00"
  "Email contact autocompletion uses only addresses from emails after this date.")

(defcustom mc-contacts-export-dir "~"
  "Directory for data exports.")

(defcustom mc-contacts-agenda-dir "~"
  "Directory in which additional contacts files are expected.")

(defcustom mc-contacts-export-name "export.vcf"
  "Default file mame for vCard exports.")

(defcustom mc-sed-binary "/bin/sed"
  "Location of the Unix sed program.")

(defcustom mc-source-dir (file-name-directory load-file-name)
  "The full path to the directory in which the sources reside.")

;; Variables that define the operation of the contacts management.
;; --------------------------------------------------------------------------------

(defconst mc-inherited-properties '("PREFIX" "FIRST" "MIDDLE" "LAST" "SUFFIX" "NICKNAME" "BIRTHDAY" "NOTE"
				    "EMAIL" "PHONE" "MOBILE" "FAX" "URL")
  "The properties of a contact that are inherited from the ancestors in the tree of entries.")

(defconst mc-property-file     "FILE"
  "The name of the (internal) contact property that gives the name of the contacts file.")

(defconst mc-property-point    "POINT"
  "The name of the (internal) contact property that gives the position in its file.")

(defconst mc-property-adr      "ADR"
  "The name of the (internal) contact property that gives the 'First Last <email@domain.com>' string.")

(defconst mc-property-type     "TYPE"
  "The name of the property that gives the type (home, work, etc.) of the contact information.")

(defconst mc-property-id       "ID"
  "The name of the property that can be added to make individuals unique")

(defconst mc-property-email    "EMAIL"
  "The name of the property that gives the email address of the contact information.")

(defconst mc-property-prefix   "PREFIX"
  "The name of the property that gives the prefix of the name.")

(defconst mc-property-first    "FIRST"
  "The name of the property that gives the first name.")

(defconst mc-property-middle   "MIDDLE"
  "The name of the property that gives the middle name.")

(defconst mc-property-last     "LAST"
  "The name of the property that gives the last name.")

(defconst mc-property-suffix   "SUFFIX"
  "The name of the property that gives the suffix of the name.")

(defconst mc-property-nickname "NICKNAME"
  "The name of the property that gives the nickname of the contact.")

(defconst mc-property-note     "NOTE"
  "The name of the property that gives the note on the contact.")

(defconst mc-property-birthday "BIRTHDAY"
  "The name of the property that gives the birthday of the contact.")

(defconst mc-property-company "COMPANY"
  "The name of the property that gives the company affiliation of the contact.")

(defconst mc-property-position "POSITION"
  "The name of the property that gives the position at that company.")

(defconst mc-property-line1    "LINE1"
  "The name of the property that gives the first line of the address of the contact.")

(defconst mc-property-line2    "LINE2"
  "The name of the property that gives the second line of the address of the contact.")

(defconst mc-property-street   "STREET"
  "The name of the property that gives the street of the contact.")

(defconst mc-property-city     "CITY"
  "The name of the property that gives the city of the contact.")

(defconst mc-property-province "PROVINCE"
  "The name of the property that gives the province of the contact.")

(defconst mc-property-postcode "POSTCODE"
  "The name of the property that gives the postcode of the contact.")

(defconst mc-property-country  "COUNTRY"
  "The name of the property that gives the country of the contact.")

(defconst mc-property-phone    "PHONE"
  "The name of the property that gives the line phone number of the contact.")

(defconst mc-property-mobile   "MOBILE"
  "The name of the property that gives the mobile phone number of the contact.")

(defconst mc-property-fax      "FAX"
  "The name of the property that gives the fax number of the contact.")

(defconst mc-property-email    "EMAIL"
  "The name of the property that gives the email address of the contact.")

(defconst mc-property-url      "URL"
  "The name of the property that gives the URL of the contact.")

(defconst mc-property-tags     "TAGS"
  "The name of the (internal) property that gives the tags string of the contact.")

(defconst mc-property-alltags  "ALLTAGS"
  "The name of the (internal) property that gives the (possibly inherited) tags string of the contact.")

;; Variables that contain the contacts data base.
;; --------------------------------------------------------------------------------

(defvar mc-database-data nil
  "The contacts data base is a list of alists. For each contacts,
  the alist contains all key value pairs that describe the properties of the contact and their values.")

(defvar mc-database-files nil
  "The list of (full paths) of all files that have been read into the contacts data base.")

;; Functions to maintain the contacts data base.
;; ================================================================================

;; Functions to parse contacts files.
;; --------------------------------------------------------------------------------

;; In order to parse contacts files, we use the following APIs of Emacs org-mode:
;; https://orgmode.org/manual/Using-the-Mapping-API.html
;; https://orgmode.org/manual/Using-the-Property-API.html

(defun mc-inherit-property (props prop point)
  "Consider the entry at <point> of an org-mode file whose
  properties have been stored in the alist <props> by
  <org-entry-properties>. If <props> does not contain a property
  <prop>, then look up this property (recursively) in the parent
  entry and return its key-value pair as an alist that can then
  be added to the property structure. If no parent defines this
  property, return <nil>."
  (when (not (cdr (assoc prop props)))
    (let ((value (org-entry-get point prop t)))
      (when value
	(cons prop value)))))

(defun mc-read-org-entry-at-point ()
  "Consider the entry at point of an org-mode file. Add key-value
  pairs for its position in its file and try to inherit all
  properties listed in <mc-inherited-properties>."
  (let* ((props (org-entry-properties))
	 (props (seq-remove (lambda (x) (string= "" (cdr x))) props))
	 (point (point)))
    (append props
	    (remove nil
		    (append (list (cons mc-property-point point))
			    (mapcar (lambda (x) (mc-inherit-property props x point)) mc-inherited-properties))))))

;; Functions to maintain the contacts data base.
;; --------------------------------------------------------------------------------

(defun mc-load-lowlevel (path)
  "Load all contacts from the given file <path> (full path
  required). Note that only org-mode entries that have the 'TYPE'
  property are loaded. Other org-mode entries are not considered
  contacts. Note that we never inherit the 'TYPE'
  peroperty. Entries without a 'TYPE' property may belogn to
  headlines of the form '* Heading' that stricture the contacts
  file, for example, and are ignored."
  (message (format "Loading contacts from '%s'. Please wait ..." path))
  (setq mc-database-data (append mc-database-data
				 (seq-remove (lambda (x) (not (cdr (assoc mc-property-type x))))
					     (org-map-entries 'mc-read-org-entry-at-point nil (list path)))))
  (message (format "Loading contacts from '%s'. Done." path)))

(defun mc-reload-file (name)
  "Remove all contacts that originate from the given file <name> and reload that file."
  (let ((path (expand-file-name name)))
    (if (member path mc-database-files)
	(setq mc-database-data (seq-remove (lambda (x) (string= path (cdr (assoc mc-property-file x)))) mc-database-data))
      (setq mc-database-files (append mc-database-files (list path))))
    (mc-load-lowlevel path)))

(defun mc-load-file (name)
  "Load all contacts from the given file <name> if not yet done."
  (let ((path (expand-file-name name)))
    (when (not (member path mc-database-files))
      (setq mc-database-files (append mc-database-files (list path)))
      (mc-load-lowlevel path))))

(defun mc-reload-database ()
  "Load all contacts from all contacts files into the data base."
  (interactive)
  (unless mc-database-files
    (setq mc-database-files (mapcar 'expand-file-name mc-contacts-files)))
  (dolist (name mc-database-files)
    (mc-reload-file name)))

(defun mc-load-database ()
  "Load all contacts from all contacts files into the data base
  if not yet done."
  (interactive)
  (unless mc-database-files
    (progn
      (setq mc-database-files (mapcar 'expand-file-name mc-contacts-files))
      (dolist (name mc-database-files)
	(mc-reload-file name))))
  (dolist (name (mapcar 'expand-file-name mc-contacts-files))
    (mc-load-file name)))

(defun mc-add-file (name)
  "Add another contacts file to the data base."
  (interactive)
  (mc-load-database)
  (unless name
    (setq name (read-file-name "Contacts file to load: " (mc-add-slash mc-contacts-agenda-dir) nil nil nil)))
  (mc-load-file name))

(defun mc-forget-file (name)
  "Remove all contacts from the data base that originate from the
  given <file>."
  (interactive)
  (mc-load-database)
  (unless name
    (setq name (read-file-name "Contacts file to forget: " (mc-add-slash mc-contacts-agenda-dir) nil nil nil)))
  (let ((path (expand-file-name name)))
    (if (member path mc-database-files)
	(progn (setq mc-database-data (seq-remove (lambda (x) (string= path (cdr (assoc mc-property-file x)))) mc-database-data))
	       (setq mc-database-files (delete path mc-database-files))
	       nil)
      (message (format "Error: The file '%s' is not part of the contacts data base." path)))))

(defun mc-dump-database ()
  "Dump the contacts data base into a temporary buffer."
  (interactive)
  (mc-load-database)
  (with-output-to-temp-buffer (generate-new-buffer "*mc-debug*")
    (print mc-database-data))
  nil)

(defun mc-save-hook ()
  "If one of the contacts files that have been read into the data
  base, has changed, remove all contacts from the data base that
  originate from this file and reload the file."
  (let ((name (buffer-file-name)))
    (when (member name mc-database-files)
      (mc-reload-file name))))

;; Whenever this module is loaded, we add a hook in order to detect changes to any of the contacts files.
(add-hook 'after-save-hook 'mc-save-hook)

;; Functions to search in the contacts data base.
;; --------------------------------------------------------------------------------

(defun mc-alist-values (alist key)
  "Return a list of all values that are associated with the given
  <key> in the associative list <alist>."
  (mapcar (lambda (x) (cdr x)) (seq-remove (lambda (x) (not (string= key (car x)))) alist)))

(defun mc-alist-condition-p (alist key value)
  "Test whether the given <alist> contains a (<key> . <value>) pair."
  (if (member value (mc-alist-values alist key)) t nil))

(defun mc-alist-conditions-p (alist conditions)
  "Test whether the given <alist> contains each key value pair
  that is given in the associative list <conditions>."
  (catch 'result
    (mapcar (lambda (x) (when (not (mc-alist-condition-p alist (car x) (cdr x))) (throw 'result nil))) conditions)
    t))

(defun mc-alist-tag-p (alist tag)
  "Test whether the given <alist> contains in its 'ALLTAGS'
  property which is a string of the form ':TAG1:TAG2:' the given
  <tag> (string)."
  (let ((p (cdr (assoc mc-property-alltags alist))))
    (set-text-properties 0 (length p) nil p)         ; remove text properties where org-mode has indicated the inheiritance
    (if p
	(string-match-p (regexp-quote (concat ":" tag ":")) p)
      nil)))

(defun mc-alist-tags-p (alist tags)
  "Test whether the given <alist> contains in its 'ALLTAGS'
  property which is a string of the form 'TAGS1:TAG2:' all of the
  given <tags> (list of strings)."
  (let ((p (cdr (assoc mc-property-alltags alist))))
    (set-text-properties 0 (length p) nil p)         ; remove text properties where org-mode has indicated the inheiritance
    (if p
	(catch 'result
	  (mapcar (lambda (x) (when (not (string-match-p (regexp-quote (concat ":" x ":")) p)) (throw 'result nil))) tags)
	  t)
      nil)))

(defun mc-contact-key (props)
  "Given the alist <prop> of all properties of a contact, return
  a string of the form 'Last|First|Middle|ID' according to which
  the contacts can be sorted."
  (concat (cdr (assoc mc-property-last props)) "|" (cdr (assoc mc-property-first props)) "|" (cdr (assoc mc-property-middle props)) "|"
	  (cdr (assoc mc-property-id props))))

(defun mc-find-property (prop)
  "Return the first contact of the data base that has a property <prop> regardless of its value."
  (catch 'result
    (mapcar (lambda (x) (when (cdr (assoc prop x)) (throw 'result x))) mc-database-data)
    nil))

(defun mc-find-condition (prop value)
  "Return the first contact of the data base whose property <prop>
  has the given <value>."
  (catch 'result
    (mapcar (lambda (x) (when (mc-alist-condition-p x prop value) (throw 'result x))) mc-database-data)
    nil))

(defun mc-find-conditions (conditions)
  "Return the first contact of the data base such that all
  properties given in the associative list <conditions> are equal
  to the respective keys in that list."
  (catch 'result
    (mapcar (lambda (x) (when (mc-alist-conditions-p x conditions) (throw 'result x))) mc-database-data)
    nil))

(defun mc-find-key (key)
  "Return the first contact of the data base that matches the
  given sort <key> 'Last|First|Middle|ID'."
  (catch 'result
    (mapcar (lambda (x) (when (string= (mc-contact-key x) key) (throw 'result x))) mc-database-data)
    nil))

(defun mc-find-key-substring (key)
  "Return the first contact of the data base whose key
  'Last|First|Middle|ID' begins with <key>."
  (catch 'result
    (mapcar (lambda (x) (let ((match (string-match-p (regexp-quote key) (mc-contact-key x))))
			  (when match (when (= 0 match) (throw 'result x))))) mc-database-data)
  nil))

(defun mc-find-tag (tag)
  "Return the first contact of the data base that contains the
  given <tag> (string) in its 'ALLTAGS' property (possibly
  inherited tags)."
  (catch 'result
    (mapcar (lambda (x) (when (mc-alist-tag-p x tag) (throw 'result x))) mc-database-data)
    nil))

(defun mc-find-tags (tags)
  "Return the first contact of the data base that contains each
  of the given <tags> (list of strings) in its 'ALLTAGS'
  proerty (possibly inherited tags)."
  (catch 'result
    (mapcar (lambda (x) (when (mc-alist-tags-p x tags) (throw 'result x))) mc-database-data)
    nil))

(defun mc-match-property (prop)
  "Return the list of all contacts of the data base that have a
  property <prop> regardless of its values."
  (seq-remove (lambda (x) (not (cdr (assoc prop x)))) mc-database-data))

(defun mc-match-condition (prop value)
  "Return the list of all contacts of the data base whose
  property <prop> has the given <value>."
  (seq-remove (lambda (x) (not (mc-alist-condition-p x prop value))) mc-database-data))

(defun mc-match-conditions (conditions)
  "Return the list of all contacts of the data base such that all
  properties given in the associative list <conditions> are equal
  to the respective keys in that list."
  (seq-remove (lambda (x) (not (mc-alist-conditions-p x conditions))) mc-database-data))

(defun mc-match-key (key)
  "Return the list of all contacts of the data base that match
  the given sort <key> 'Last|First|Middle|ID'."
  (seq-remove (lambda (x) (not (string= (mc-contact-key x) key))) mc-database-data))

(defun mc-match-key-substring (key)
  "Return the list of all contacts of the data base whose key
  'Last|First|Middle|ID' begins with <key>."
  (seq-remove (lambda (x) (let ((match (string-match-p (regexp-quote key) (mc-contact-key x))))
			    (if match (not (= 0 match)) t))) mc-database-data))
(defun mc-match-tag (tag)
  "Return the list of all contacts of the data base that contain
  the given <tag> in their 'ALLTAGS' property (possibly inherited
  tags)."
  (seq-remove (lambda (x) (not (mc-alist-tag-p x tag)))
	      mc-database-data))

(defun mc-match-tags (tags)
  "Return the list of all contacts of the data base that contain
each of the given <tags> in their 'ALLTAGS' property (possibly
inherited tags)."
  (seq-remove (lambda (x) (not (mc-alist-tags-p x tags))) mc-database-data))

(defun mc-database-sorted ()
  "Return a sorted list of all contacts of the data base that have a 'TYPE' property."
  (sort (copy-list mc-databse-data)) (lambda (a b) (string< (mc-contact-key a) (mc-contact-key b))))

;; Functions to export contacts.
;; ================================================================================

;; Export of vCards.
;; --------------------------------------------------------------------------------

(defun mc-vcard-name (props)
  "For a contact with properties alist <prop> return the name
  field of its vCard entry."
  (concat "N:" (cdr (assoc mc-property-last props)) ";" (cdr (assoc mc-property-first props)) ";" (cdr (assoc mc-property-middle props))
	  ";" (cdr (assoc mc-property-prefix props)) ";" (cdr (assoc mc-property-suffix props)) "\n"))

(defun mc-vcard-fname (props)
  "For a contact with properties alist <prop> return the
  formatted name field of its vCard entry."
  (concat "FN:"
	  (let ((p (cdr (assoc mc-property-prefix props)))) (when p (concat p " ")))
	  (let ((p (cdr (assoc mc-property-first  props)))) (when p (concat p " ")))
	  (let ((p (cdr (assoc mc-property-middle props)))) (when p (concat p " ")))
	  (cdr (assoc mc-property-last props))
	  (let ((p (cdr (assoc mc-property-middle props)))) (when p (concat ", " p)))
	  "\n"))

(defun mc-vcard-tags (props)
  "For a contact with properties alist <prop> return the
  categories field of its vCard."
  (let ((tags (cdr (assoc mc-property-alltags props))))
    (set-text-properties 0 (length tags) nil tags)      ; remove text properties where org-mode has indicated the inheiritance
    (when tags (concat
		"CATEGORIES:"
		(apply 'concat
		       (cdr (apply 'append
				   (mapcar (lambda (x) (list "," x))
					   (seq-filter (lambda (y) (not (string= "" y))) (split-string tags ":"))))))
		"\n"))))

(defun mc-vcard-nickname (props)
  "For a contact with properties alist <props> return the
  nickname field of its vCard."
  (let ((p (cdr (assoc mc-property-nickname props))))
    (when p (concat "NICKNAME:" p "\n"))))

(defun mc-vcard-note (props)
  "For a contact with properties alist <props> return the
  note field of its vCard."
  (let ((p (cdr (assoc mc-property-note props))))
    (when p (concat "NOTE:" p "\n"))))

(defun mc-vcard-bday (props)
  "For a contact with properties alist <props> return the
  birthday field of its vCard."
  (let ((p (cdr (assoc mc-property-birthday props))))
    (when p (concat "BDAY:" p "\n"))))

(defun mc-export-vcard-type (props)
  "For a contact with properties alist <props> return a string
  that contains all field of its vCard that depend on the 'TYPE'."
  (let ((type (cdr (assoc mc-property-type props))))
    (concat
     (let ((p (cdr (assoc mc-property-street props))))
       (when p (concat "ADR;" type ":"
		       (cdr (assoc mc-property-line1 props)) ";"
		       (cdr (assoc mc-property-line2 props)) ";"
		       p ";"
		       (cdr (assoc mc-property-city props)) ";"
		       (cdr (assoc mc-property-province props)) ";"
		       (cdr (assoc mc-property-postcode props)) ";"
		       (cdr (assoc mc-property-country props)) "\n")))
     (let ((p (cdr (assoc mc-property-email props))))
       (when p (concat "EMAIL;" type ":" p "\n")))
     (let ((p (cdr (assoc mc-property-phone props))))
       (when p (concat "TEL;" type ":" p "\n")))
     (let ((p (cdr (assoc mc-property-mobile props))))
       (when p (concat "TEL;" type ";CELL:" p "\n")))
     (let ((p (cdr (assoc mc-property-fax props))))
       (when p (concat "TEL;" type ";FAX:" p "\n")))
     (let ((p (cdr (assoc mc-property-url props))))
       (when p (concat "URL;" type ":" p "\n"))))))

(defun mc-export-vcard (key)
  "For a contact that matches the given search <key>, return its
  vCard as a string."
  (let* ((contacts (mc-match-key key))
	 (first    (car contacts)))
    (concat "BEGIN:VCARD\nVERSION:2.1\n"
	    (mc-vcard-name     first)
	    (mc-vcard-fname    first)
	    (mc-vcard-tags     first)
	    (mc-vcard-nickname first)
	    (mc-vcard-note     first)
	    (mc-vcard-bday     first)
	    (mapconcat 'mc-export-vcard-type contacts "")
	    "END:VCARD\n\n")))

(defun mc-add-slash (s)
  "Add a slash '/' if not already present."
  (if (string-suffix-p "/" s) s (concat s "/")))

(defun mc-export-vcf-contacts (contacts)
  "Given a list of <contacts>, i.e. a list of alists each of
  which describes the properties of a contact, write a .vcf file
  that contains the vCards of these contacts. Return the file
  name selected by the user."
  (interactive)
  (mc-load-database)
  (let ((vcf (expand-file-name (read-file-name "Output .vcf file to: " (mc-add-slash mc-contacts-export-dir) nil nil mc-contacts-export-name) "~")))
    (with-temp-file vcf
      (apply 'insert (mapcar 'mc-export-vcard (delete-dups (sort (mapcar 'mc-contact-key contacts) 'string<))))
      ) vcf))

(defun mc-export-vcf-tags ()
  "Write a .vcf file that contains the vCards of all contacts
  that have each of the user specified tags in their 'ALLTAGS'
  property (possibly inherited tags). Return the file name
  selected by the user."
  (interactive)
  (mc-load-database)
  (let* ((tagstring (read-from-minibuffer "Filter for tags: " ":VCF:"))
	 (tags (seq-filter (lambda (y) (not (string= "" y))) (split-string tagstring ":"))))
    (mc-export-vcf-contacts (mc-match-tags tags))))

(defun mc-export-vcf ()
  "Export all contacts to a .vcf file for transfer to the
  phone. The user is prompted for the file name, and this
  function returns the absolute path of the file that was
  written."
  (interactive)
  (mc-load-database)
  (mc-export-vcf-contacts mc-database-data))

;; Export of XML for viewing in a browser and for printing.
;; --------------------------------------------------------------------------------

(defun mc-xml-quote (s)
  "Quote a string for inclusion in XML."
  (replace-regexp-in-string ">" "&gt;" (replace-regexp-in-string "<" "&lt;" (replace-regexp-in-string "&" "&amp;" s))))

(defun mc-xml-tags (props)
  "For a contact with properties alist <props> return an XML
  representation of its tags."
  (let ((tags (cdr (assoc mc-property-alltags props))))
    (set-text-properties 0 (length tags) nil tags)      ; remove text properties where org-mode has indicated the inheiritance
    (when tags (concat
		"    <mc:tags>\n"
		(mapconcat (lambda (x) (concat "      <mc:tag>" (mc-xml-quote x) "</mc:tag>\n"))
			   (seq-filter (lambda (y) (not (string= "" y))) (split-string tags ":")) "")
		"    </mc:tags>\n"))))

(defun mc-xml-property (props prop prefix)
  "For a contact with properties alist <props> return an XML
  representation of the property <prop>. Here <prefix> is a
  string that is returned at the beginning of each line."
  (let ((p (cdr (assoc prop props))))
    (when p (concat prefix "<mc:" (downcase prop) ">" (mc-xml-quote p) "</mc:" (downcase prop) ">\n"))))

(defun mc-export-xml-type (props)
  "For a contact with properties alist <props> return a string
  with an XML representation of its 'TYPE' dependent properties."
  (let ((type (cdr (assoc mc-property-type props))))
    (concat "    <mc:block>\n      <mc:type>" (mc-xml-quote type) "</mc:type>\n"
	    (mapconcat (lambda (x) (mc-xml-property first x "      "))
		       (list mc-property-line1 mc-property-line2 mc-property-street mc-property-city mc-property-province
			     mc-property-postcode mc-property-country
			     mc-property-phone mc-property-mobile mc-property-fax mc-property-email mc-property-url) "")
	    "    </mc:block>\n")))

(defun mc-export-xml (key)
  "For a contact that matches the given search <key>, return a
  string with an XML representation of it."
  (let* ((contacts (mc-match-key key))
	 (first    (car contacts)))
    (concat "  <mc:contact>\n"
	    (mapconcat (lambda (x) (mc-xml-property first x "    "))
		       (list mc-property-prefix mc-property-first mc-property-middle mc-property-last mc-property-suffix
			     mc-property-nickname mc-property-note mc-property-birthday) "")
	    (mc-xml-tags     first)
	    (mapconcat 'mc-export-xml-type contacts "")
	    "  </mc:contact>\n")))

(defun mc-export-xmlfile-contacts (contacts)
  "Given a list of <contacts>, i.e. a list of alists each of
  which describes the properties of a contact, write an XML
  representation to a temporary file and return its full path."
  (let ((temp (concat (make-temp-file "contacts-xml-") ".xml"))
	(header (concat (mc-add-slash mc-source-dir) "mu4e-contacts-xml-header.xml"))
	(style  (concat (mc-add-slash mc-source-dir) "mu4e-contacts-xml-style.xml")))
    (with-temp-file temp
      (insert (with-temp-buffer (insert-file-contents header) (buffer-string)))
      (insert "<mc:contacts xmlns:mc=\"http://www.mu4e-contacts.org/Namespac/1.0/\">\n")
      (insert (with-temp-buffer (insert-file-contents style) (buffer-string)))
      (insert (mapconcat 'mc-export-xml (delete-dups (sort (mapcar 'mc-contact-key contacts) 'string<)) ""))
      (insert "</mc:contacts>\n"))
    temp))

(defun mc-export-xmlfile-tags ()
  "Write an XML representation to a temporary file that contains
  all contacts that have the user specified tags in their
  'ALLTAGS' property (possibly inherited tags)."
  (let* ((tagstring (read-from-minibuffer "Filter for tags: " ":VCF:"))
	 (tags (seq-filter (lambda (y) (not (string= "" y))) (split-string tagstring ":"))))
    (mc-export-xmlfile-contacts (mc-match-tags tags))))

(defun mc-browse-xmlfile-contacts (contacts)
  "Given a list of <contacts>, i.e. a list of alists each of
  which describes the properties of a contact, open an XML
  representation in the web browser."
  (interactive)
  (browse-url (concat "file://" (mc-export-xmlfile-contacts contacts))))

(defun mc-browse-xmlfile-tags ()
  "Open an XML representation in the web browser that contains
  all contacts that have the user specified tags in their
  'ALLTAGS' property (possibly inherited tags)."
  (interactive)
  (browse-url (concat "file://" (mc-export-xmlfile-tags))))

(defun mc-browse-xmlfile ()
  "Open an XML representation in the web browser of all contacts."
  (interactive)
  (browse-url (concat "file://" (mc-export-xmlfile-contacts mc-database-data))))

;; Functions to improve mu4e's email address autocompletion.
;; --------------------------------------------------------------------------------

(defun mc-nice-plain (props)
  "Given the alist <props> of the properties of a contact,
  produce an email address entry of the form
  'Dr. First Middle Last, CFA <email@domain.com>'.
  If there is no 'EMAIL' property, the function return nil."
  (when (cdr (assoc mc-property-email props))
    (concat (let ((p (cdr (assoc mc-property-prefix props)))) (when p (concat p " ")))
	    (let ((p (cdr (assoc mc-property-first props)))) (when p (concat p " ")))
	    (let ((p (cdr (assoc mc-property-middle props)))) (when p (concat p " ")))
	    (cdr (assoc mc-property-last props))
	    (let ((p (cdr (assoc mc-property-suffix props)))) (when p (concat " (" p ")")))
	    " <" (cdr (assoc mc-property-email props)) ">")))

;; The email address autocompletion of mu4e is extended as follows. In plain mu4e, a list of potential email addresses
;; is extracted from the command line tool 'mu'. In addition to this list, we extract potential email addresses from the
;; contacts database as well and supply them at the beginning of the list of candidates, i.e. email adresses from the
;; contacts database are suggested first.

;; If we work with mu4e until version 1.2, we need to replace the function <mu4e~fill-contacts> in order to sneak in our
;; suggested contacts.

;; Add a string of the form "First Last <email@domain.com>" to the email address autocompletion hash table.
(defun mc-autocomplete-favourite (string)
  "Add favourite email address <string>."
  (puthash string (+ (hash-table-count mu4e~contacts) 1) mu4e~contacts))

(defun mu4e~fill-contacts (contact-data)
  "We receive a list of contacts, which each contact of the form
  (:name NAME :mail EMAIL :tstamp TIMESTAMP :freq FREQUENCY)
and fill the hash  `mu4e~contacts-for-completion' with it, with
each contact mapped to an integer for their ranking.

This is used by the completion function in mu4e-compose."
  (let ((contacts) (rank 0))

    ;; First obtain the usual autocompletion list from "mu cfind".
    (dolist (contact contact-data)
      (let ((contact-maybe (mu4e~process-contact contact)))
	;; note, this gives cells (rfc822-address . contact)
	(when contact-maybe (push contact-maybe contacts))))
    (setq contacts (mu4e~sort-contacts contacts))

    ;; Now, we have our nicely sorted list, map them to a list of increasing integers. We use that map in the composer
    ;; to sort them there. It would have been so much easier if emacs allowed us to use the sorted-list as-is, but no
    ;; such luck.
    (setq mu4e~contacts (make-hash-table :test 'equal :weakness nil
					 :size (length contacts)))

    ;; Then we add the list of favourites that we compile from our contacts.org file.
    (mc-load-database)
    (setq favourites (remove nil (mapcar 'mc-nice-plain mc-database-data)))
    (mapc 'mc-autocomplete-favourite favourites)

    ;; Finally, the has is prepared for email address autocompletion.
    (dolist (contact contacts)
      (puthash (car contact) rank mu4e~contacts)
      (incf rank))
    (mu4e-index-message "Contacts received: %d"
      (hash-table-count mu4e~contacts))))

;; If we work with mu4e from version 1.4, however, it is a different function <mu4e~update-contacts> that needs to be
;; modified.

(defun mu4e~update-contacts (contacts &optional tstamp)
  "Receive a sorted list of CONTACTS.
Each of the contacts has the form
  (FULL_EMAIL_ADDRESS . RANK) and fill the hash
`mu4e~contacts' with it, with each contact mapped to an integer
for their ranking.

This is used by the completion function in mu4e-compose."
  ;; We have our nicely sorted list, map them to a list
  ;; of increasing integers. We use that map in the composer
  ;; to sort them there. It would have been so much easier if emacs
  ;; allowed us to use the sorted-list as-is, but no such luck.
  (let ((n 0))
    (unless mu4e~contacts
      (setq mu4e~contacts (make-hash-table :test 'equal :weakness nil
                                           :size (length contacts))))

    ;; First we add the list of favourites that we have explicitly compiled from our contacts.org file.
    (mc-load-database)
    (setq favourites (remove nil (mapcar 'mc-nice-plain mc-database-data)))
    (mapc 'mc-autocomplete-favourite favourites)

    ;; Then we continue as in the original function.
    (dolist (contact contacts)
      (cl-incf n)
      (let ((address
             (if (functionp mu4e-contact-process-function)
                 (funcall mu4e-contact-process-function (car contact))
               (car contact))))
        (when address ;; note the explicit deccode; the strings we get are  utf-8,
          ;; but emacs doesn't know yet.
          (puthash (decode-coding-string address 'utf-8) (cdr contact) mu4e~contacts))))

    (setq mu4e~contacts-tstamp (or tstamp "0"))

    (unless (zerop n)
      (mu4e-index-message "Contacts updated: %d; total %d"
                          n (hash-table-count mu4e~contacts)))))

;; Functions to construct helm sources from the data base.
;; --------------------------------------------------------------------------------

(defun mc-nice-long (props)
  "Given the alist <props> of the properties of a contact,
  produce a helm source string of the form
  'Dr. First (Nickname) Middle Last, CFA <email@domain.com> :TAG1:TAG2:'
  so that helm's fuzzy search can not only pick up pieces of the
  name or the email address, but also a potential nickname or
  tags.  If there is no 'EMAIL' property, the function return
  nil."
  (when (cdr (assoc mc-property-email props))
    (format "%-40s %s"
	    (concat (when (cdr (assoc mc-property-prefix props)) (concat (cdr (assoc mc-property-prefix props)) " "))
		    (when (cdr (assoc mc-property-first props)) (concat (cdr (assoc mc-property-first props)) " "))
		    (when (cdr (assoc mc-property-nickname props)) (concat "(" (cdr (assoc mc-property-nickname props)) ") " ))
		    (when (cdr (assoc mc-property-middle props)) (concat (cdr (assoc mc-property-middle props)) " "))
		    (cdr (assoc mc-property-last props))
		    (when (cdr (assoc mc-property-suffix props)) (concat ", " (cdr (assoc mc-property-suffix props)))))
	    (concat "<" (cdr (assoc mc-property-email props)) ">"
		    (when (cdr (assoc mc-property-tags props)) (concat " " (cdr (assoc mc-property-tags props))))))))

(defun mc-adr-to-email (s)
  "Given a string of the form 'First Last <email@domain.com>', return 'email@domain.com'."
  (if (string-match ".*<\\(.*\\)>" s) (match-string 1 s) nil))

(defun mc-adr-format (s)
  "Given a string of the form 'First Last <email@domain.com>', format it."
  (if (string-match "\\(.*\\) <\\(.*\\)>" s)
      (format "%-40s <%s>" (match-string 1 s) (match-string 2 s))
    (format "%40s <%s>" "" s)))

(defun mc-helm-source (nice)
  "For all contacts from the data base that have an 'EMAIL'
  property, return an alist that has the string prodiced by <nice> as the key and
  the propery alist that describes the contact, as the value."
  (let ((sublist (mc-match-property mc-property-email)))
    (mapcar (lambda (x) (cons (funcall nice x) (list x))) sublist)
    ))

(defun mc-helm-email-source-1 ()
  "Return an alist with elements of the form
   ('Dr. First Last, PhD <firstlast@company.org> :TAG:ANOTHER:' . <properties>)
  where <properties> is the full alist that describes the
  properties of the contact. The function returns these elements
  for all contacts from the data base that have an 'EMAIL'
  property."
  (mc-helm-source 'mc-nice-long))

(defun mc-helm-email-source-2 ()
  "Return an alist with elements of the form
   ('Dr. First Last, PhD <firstlast@company.org> :TAG:ANOTHER:'
  . <properties>) where <properties> is the full alist that
  describes the properties of the contact. The function returns
  these elements for all email addresses that mu extracts from
  the personal emails. The <properties> structure has merely the
  'EMAIL' property and an additional internal 'ADR' that contains
  the 'Name <email@domain.com>' that was returned from 'mu'."
  (let ((cmd (concat mu4e-mu-binary " cfind --muhome=" mu4e-mu-home " --format=csv --personal"
		     (format " --after=%d" (truncate (float-time (date-to-time mc-compose-complete-only-after))))
		     " | " mc-sed-binary " 's/\\(.*\\),\\(.*\\)/\\1 <\\2>/'")))
    (mapcar (lambda (s) ;; ... and construct the alist with the formatted email address on the left and the single property 'EMAIL' alist on the right.
	      (cons (mc-adr-format s) (list (list (cons mc-property-email (mc-adr-to-email s))
						  (cons mc-property-adr s)))))
	    (split-string (shell-command-to-string cmd) "\n"))))

(defun mc-helm-email-source-3 ()
  "Return an alist with elements of the form
   ('Dr. First Last, PhD <firstlast@company.org> :TAG:ANOTHER:'
  . <properties>) where <properties> is the full alist that
  describes the properties of the contact. The function returns
  these elements for all email addresses that mu extracts from
  all emails. The <properties> structure has merely the 'EMAIL'
  property and an additional internal 'ADR' that contains the
  'Name <email@domain.com>' that was returned from 'mu'."
  (let ((cmd (concat mu4e-mu-binary " cfind --muhome=" mu4e-mu-home " --format=csv "
		     (format " --after=%d" (truncate (float-time (date-to-time mc-compose-complete-only-after))))
		     " | " mc-sed-binary " 's/\\(.*\\),\\(.*\\)/\\1 <\\2>/'")))
    (mapcar (lambda (s) ;; ... and construct the alist with the formatted email address on the left and the single property 'EMAIL' alist on the right.
	      (cons (mc-adr-format s) (list (list (cons mc-property-email (mc-adr-to-email s))
						  (cons mc-property-adr s)))))
	    (split-string (shell-command-to-string cmd) "\n"))))

(defun mc-helm-candidates-email ()
  "From the candidates selected in helm, produce a list of plain email addresses."
  (remove nil (mapcar (lambda (x) (cdr (assoc mc-property-email x)))
		      ;; the result of helm-marked-candidates has one list nesting too many, and so we (apply 'append ...)
		      ;; in order to flatten it
		      (apply 'append (helm-marked-candidates :all-sources t)))))

(defun mc-helm-candidates-string ()
  "From the candidates selected in helm, produce a string of the comma separated email contacts."
  (mapconcat 'identity ;; insert a comma separated list of email contacts
	     (mapcar (lambda (x) (if (cdr (assoc mc-property-adr x))
				     (cdr (assoc mc-property-adr x)) ;; if mu has supplied an email contact 'First Last <email@domain.com' we use it
				   (mc-nice-plain x)))               ;; otherwise we take the email address we have and format it
		     ;; the result of helm-marked-candidates has one list nesting too many, and so we (apply 'append ...)
		     ;; in order to flatten it
		     (apply 'append (helm-marked-candidates :all-sources t)))
	     ", "))

(defun mc-helm-action-insert (candidate)
  "Helm action that inserts a comma separated list of email contacts at point."
  (insert (mc-helm-candidates-string)))

(defun mc-remove-if-exists (a b)
  "Remove all elements from list <b> that are contained in list <a>."
  (let ((result nil))
    (while b
      (when (and (car b) (not (member (car b) a)))
	(setq result (cons (car b) result)))
      (setq b (cdr b)))
    (nreverse result)))

(defun mc-helm-email-insert ()
  "Helm selection of email contacts which are then inserted as a
  comma separated list of strings at point."
  (interactive)
  (mc-load-database)
  (let* ((source1 (mc-helm-email-source-1))
	 (source2 (mc-helm-email-source-2))
	 (source3 (mc-remove-if-exists source2 (mc-helm-email-source-3))))
    (helm :sources (list (helm-build-sync-source "Email Contacts from the Data Base"
			   :candidates source1
			   :fuzzy-match t
			   :action #'mc-helm-action-insert)
			 (helm-build-sync-source "Email Contacts from Personal Emails"
			   :candidates source2
			   :fuzzy-match t
			   :action #'mc-helm-action-insert)
			 (helm-build-sync-source "Email Contacts from All Emails"
			   :candidates source3
			   :fuzzy-match t
			   :action #'mc-helm-action-insert))
	  :buffer "*helm email contacts insert*")))

(defun mc-helm-action-compose (candidate)
  "Helm action that composes a new email adressed to the selected contacts."
  (mu4e~compose-mail (mc-helm-candidates-string))
  (mu4e-compose-mode))

(defun mc-helm-action-search (candidate)
  "Helm action to search for any correspondence with the selected contacts."
  (mu4e-headers-search (concat "(contact:" (mapconcat 'identity (mc-helm-candidates-email) " OR contact:") ")")))

(defun mc-helm-action-fromto (candidate)
  "Helm action to search for messages from or to the selected contacts."
  (mu4e-headers-search (concat "(from:" (mapconcat 'identity (mc-helm-candidates-email) " OR from:")
			       "OR to:" (mapconcat 'identity (mc-helm-candidates-email) " OR to:") ")")))

(defun mc-helm-action-jumpto (candidate)
  "Helm action to jump to the definition of the first selected contact in one of the contacts files."
  (let ((props (car (apply 'append (helm-marked-candidates :all-sources t)))))
    (if (cdr (assoc mc-property-point props))
	(progn
	  (find-file (cdr (assoc mc-property-file props)))
	  (goto-char (cdr (assoc mc-property-point props)))
	  (show-subtree))
      (message "This email address is not contained in any contacts file."))))

(defun mc-helm-action-vcf (candidate)
  "Helm action to export vCards of the selected contacts into a
  .vcf file."
  (mc-export-vcf-contacts (apply 'append (helm-marked-candidates :all.sources t))))

(defun mc-helm-email-menu ()
  "Helm Selection of Email Contacts."
  (interactive)
  (mc-load-database)
  (let* ((source1 (mc-helm-email-source-1))
	 (source2 (mc-helm-email-source-2))
	 (source3 (mc-remove-if-exists source2 (mc-helm-email-source-3))))
    (helm :sources (list (helm-build-sync-source "Email Contacts from the Data Base"
			   :candidates source1
			   :fuzzy-match t
			   :action #'(("Compose Email"                 . mc-helm-action-compose)
				      ("Insert Email Addresses"        . mc-helm-action-insert)
				      ("Search Correspondence"         . mc-helm-action-search)
				      ("Search From/To Fields"         . mc-helm-action-fromto)
				      ("Jump To Contact"               . mc-helm-action-jumpto)
				      ("Export vCards"                 . mc-helm-action-vcf)))
			 (helm-build-sync-source "Email Contacts from Personal Emails"
			   :candidates source2
			   :fuzzy-match t
			   :action #'(("Compose Email"                 . mc-helm-action-compose)
				      ("Insert Email Addresses"        . mc-helm-action-insert)
				      ("Search Correspondence"         . mc-helm-action-search)
				      ("Search From/To Fields"         . mc-helm-action-fromto)))
			 (helm-build-sync-source "Email Contacts from All Emails"
			   :candidates source3
			   :fuzzy-match t
			   :action #'(("Compose Email"                 . mc-helm-action-compose)
				      ("Insert Email Addresses"        . mc-helm-action-insert)
				      ("Search Correspondence"         . mc-helm-action-search)
				      ("Search From/To Fields"         . mc-helm-action-fromto))))
	  :buffer "*helm email contacts menu*")))

(defun mc-nice-full (props)
  "Given the alist <props> of the properties of a contact,
  produce a helm source string in multi-line form."
  (concat (cdr (assoc mc-property-last props))
	  (if (or (cdr (assoc mc-property-prefix props))
		    (cdr (assoc mc-property-first props))
		    (cdr (assoc mc-property-nickname props))
		    (cdr (assoc mc-property-middle props))
		    (cdr (assoc mc-property-suffix props))
		    (cdr (assoc mc-property-tags props)))
	      (concat ","
		      (when (cdr (assoc mc-property-prefix props)) (concat " " (cdr (assoc mc-property-prefix props))))
		      (when (cdr (assoc mc-property-first props)) (concat " " (cdr (assoc mc-property-first props))))
		      (when (cdr (assoc mc-property-nickname props)) (concat " (" (cdr (assoc mc-property-nickname props))))
		      (when (cdr (assoc mc-property-middle props)) (concat " " (cdr (assoc mc-property-middle props))))
		      (when (cdr (assoc mc-property-suffix props)) (concat ", " (cdr (assoc mc-property-suffix props))))
		      (when (cdr (assoc mc-property-tags props)) (concat " " (cdr (assoc mc-property-tags props)))))
	    "")
	  (if (or (cdr (assoc mc-property-position props))
		  (cdr (assoc mc-property-company props)))
	      (concat "; "
		      (when (cdr (assoc mc-property-position props)) (concat (cdr (assoc mc-property-position props)) " at "))
		      (cdr (assoc mc-property-company props)))
	    "")
	  (if (cdr (assoc mc-property-birthday props)) (concat "; born " (cdr (assoc mc-property-birthday props))) "")
	  "\n  " (cdr (assoc mc-property-type props)) ": "
	  (when (cdr (assoc mc-property-street props))
	    (concat (when (cdr (assoc mc-property-line1 props)) (concat (cdr (assoc mc-property-line1 props)) ", "))
		    (when (cdr (assoc mc-property-line2 props)) (concat (cdr (assoc mc-property-line2 props)) ", "))
		    (cdr (assoc mc-property-street props))
		    (when (cdr (assoc mc-property-postcode props)) (concat ", " (cdr (assoc mc-property-postcode props))))
		    (when (cdr (assoc mc-property-city props)) (concat ", " (cdr (assoc mc-property-city props))))
		    (when (cdr (assoc mc-property-province props)) (concat ", " (cdr (assoc mc-property-province props))))
		    (when (cdr (assoc mc-property-country props)) (concat ", " (cdr (assoc mc-property-country props))))
		    "\n    " (make-string (length (cdr (assoc mc-property-type props))) ? )))
	  (when (cdr (assoc mc-property-phone props)) (concat "phone: " (cdr (assoc mc-property-phone props)) " "))
	  (when (cdr (assoc mc-property-mobile props)) (concat "mobile: " (cdr (assoc mc-property-mobile props)) " "))
	  (when (cdr (assoc mc-property-mobile props)) (concat "fax: " (cdr (assoc mc-property-fax props)) " "))
	  (when (cdr (assoc mc-property-mobile props)) (concat "email: " (cdr (assoc mc-property-email props)) " "))))

(defun mc-helm-full-source ()
  "Return an alist whose elements have a multi-line string that
  describes the contact as their key and the full alist that
  describes all properties on the contact as their value."
  (mapcar (lambda (x) (cons (mc-nice-full x) (list x)))
	  (mapcan 'mc-match-key (delete-dups (sort (mapcar 'mc-contact-key mc-database-data) 'string<)))))

(defun mc-helm-full-menu ()
  "Helm Browser of Contacts."
  (interactive)
  (mc-load-database)
  (helm :sources (helm-build-sync-source "Select Contacts"
		   :candidates #'mc-helm-full-source
		   :multiline t
		   :fuzzy-match t
                   :action #'(("Jump To Contact"               . mc-helm-action-jumpto)
			      ("Compose Email"                 . mc-helm-action-compose)
			      ("Insert Email Addresses"        . mc-helm-action-insert)
			      ("Search Correspondence"         . mc-helm-action-search)
			      ("Search From/To Fields"         . mc-helm-action-fromto)
			      ("Export vCards"                 . mc-helm-action-vcf)))
	:buffer "*helm contacts menu*"))

;; Capture Templates.
;; ================================================================================

(defun mc-capture-template-bind (letter)
  "Bind the capture template for new contacts to the given
  <letter> in the menu of all capture templates."
  (add-to-list 'org-capture-templates
	       (list letter "Contact" 'entry '(file+headline mc-capture-file "New Contacts")
		 "\n*** %\\2 %\\3 %\\4%? %^g\n   :PROPERTIES:\n   :PREFIX:   %^{Name Prefix}\n   :FIRST:    %^{First Name}\n   :MIDDLE:   %^{Middle Name}\n   :LAST:     %^{Last Name}\n   :SUFFIX:   %^{Name Suffix}\n   :NICKNAME: \n   :NOTE:     \n   :BIRTHDAY: %^{Birthday}t\n   :TYPE:     \n   :COMPANY:    \n   :POSITION:   \n   :LINE1:    \n   :LINE2:    \n   :STREET:   \n   :CITY:     \n   :POSTCODE: \n   :PROVINCE: \n   :COUNTRY:  \n   :EMAIL:    %^{E-Mail}\n   :PHONE:    \n   :MOBILE:   \n   :FAX:      \n   :URL:      \n   :ADDED:    %U\n   :END:" :empty-lines-before 1 :empty-lines-after 1)))

(provide 'mu4e-contacts)

;;; ================================================================================
;;; End of file.
;;; ================================================================================
